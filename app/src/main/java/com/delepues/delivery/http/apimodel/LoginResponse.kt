package com.delepues.delivery.http.apimodel

/**
 * Created by azapata on 10/31/17.
 */

class LoginResponse(var user: User, var token: String, var success: Boolean)
