package com.delepues.delivery.http

import com.delepues.delivery.http.apimodel.LocationDriver
import com.delepues.delivery.http.apimodel.Login
import com.delepues.delivery.http.apimodel.LoginResponse
import com.delepues.delivery.http.apimodel.OrderNotification
import retrofit2.Response

import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import rx.Observable

/**
 * Created by azapata on 9/25/17.
 */

interface DeliveryApiService {

    @POST("auth")
    fun loginUser(@Body login: Login): Observable<Response<LoginResponse>>


//    @POST("drivers/{driverId}/locations")
//    fun publishLocation(@Body locationDriver: LocationDriver, @Path("driverId") driverId: String): Observable<LocationDriver>

    @POST("drivers/{driverId}/locations")
    fun publishLocation(@Header("dpx-access-token") token: String, @Body locationDriver: LocationDriver, @Path("driverId") driverId: String): Observable<LocationDriver>

    @POST("drivers/{driverId}/orders/{orderId}")
    fun actionOrder(@Header("dpx-access-token") token: String, @Body orderNotification: OrderNotification, @Path("driverId") driverId: String, @Path("orderId") orderId: String): Observable<LocationDriver>
}