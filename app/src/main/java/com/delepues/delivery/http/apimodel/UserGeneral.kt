package com.delepues.delivery.http.apimodel

/**
 * Created by azapata on 10/31/17.
 */

class UserGeneral(var user_id: String, var user_state_id: String, var username: String, var email: String, var first_name: String, var last_name: String, var description: String)