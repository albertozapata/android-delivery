package com.delepues.delivery.http.apimodel

/**
 * Created by azapata on 10/31/17.
 */

class User {
    var driver_id: String? = null
    var general: UserGeneral? = null
    var profile: String? = null
}
