package com.delepues.delivery.http.apimodel

/**
 * Created by azapata on 12/21/17.
 */
class LocationDriver(val latitude: Double, val longitude: Double)