package com.delepues.delivery.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.delepues.delivery.R;
import com.delepues.delivery.http.apimodel.Login;
import com.delepues.delivery.http.apimodel.LoginResponse;
import com.delepues.delivery.main.MainActivity;
import com.delepues.delivery.root.App;
import com.delepues.delivery.utils.Constants;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.basgeekball.awesomevalidation.ValidationStyle.UNDERLABEL;

public class LoginActivity extends AppCompatActivity implements LoginActivityMVP.View {

    @BindView(R.id.edit_user_login)
    EditText userName;

    @BindView(R.id.edit_pass_login)
    EditText password;

    @BindView(R.id.button_login)
    Button login;
    private AwesomeValidation mAwesomeValidation;

    @Inject
    LoginActivityMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        ((App) getApplication()).getComponent().inject(this);

        mAwesomeValidation = new AwesomeValidation(UNDERLABEL);
        mAwesomeValidation.setContext(this);  // mandatory for UNDERLABEL style
        mAwesomeValidation.addValidation(this, R.id.edit_user_login, Patterns.EMAIL_ADDRESS, R.string.error_email);
        mAwesomeValidation.addValidation(this, R.id.edit_pass_login, ".{8,}", R.string.error_password);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

    }

    private void validate() {
        if (mAwesomeValidation.validate()) {
            presenter.loginUser(new Login(userName.getText().toString().trim(), password.getText().toString().trim()));
        }
    }

    @Override
    public void showUser(LoginResponse response) {
        Intent intent = new Intent(this, MainActivity.class);
        Gson gson = new Gson();
        String json = gson.toJson(response);
        Prefs.putString(Constants.profile, json);
        Prefs.putString(Constants.userEmail, response.getUser().getGeneral().getEmail());
        Prefs.putString(Constants.token, response.getToken());
        Prefs.putString(Constants.driverId, response.getUser().getDriver_id());
        startActivity(intent);
        this.finish();
    }

    @Override
    public void errorLogin() {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void errorEmpty() {

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.rxUnsubscribe();
    }
}
