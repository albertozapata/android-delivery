package com.delepues.delivery.login;

import com.delepues.delivery.http.apimodel.Login;
import com.delepues.delivery.http.apimodel.LoginResponse;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public interface Repository {
    Observable<Response<LoginResponse>> getResultsFromNetwork(Login login);
    Observable<Response<LoginResponse>> getResultsFromLocal(Login login);
}
