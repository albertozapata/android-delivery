package com.delepues.delivery.login;


import com.delepues.delivery.http.apimodel.Login;
import com.delepues.delivery.http.apimodel.LoginResponse;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class LoginModel implements LoginActivityMVP.Model {
    private Repository repository;

    public LoginModel(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<Response<LoginResponse>> loginUser(Login login) {
        return repository.getResultsFromLocal(login);
    }
}
