package com.delepues.delivery.login;

import com.delepues.delivery.http.apimodel.Login;
import com.delepues.delivery.http.apimodel.LoginResponse;

import retrofit2.Response;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by azapata on 10/12/17.
 */

public class LoginPresenter implements LoginActivityMVP.Presenter {
    private LoginActivityMVP.View view;
    private Subscription subscription = null;
    private LoginActivityMVP.Model model;
    private LoginResponse response;

    public LoginPresenter(LoginActivityMVP.Model model) {
        this.model = model;
    }


    @Override
    public void loginUser(Login login) {
        subscription = model.loginUser(login).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<LoginResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.errorEmpty();
                    }

                    @Override
                    public void onNext(Response<LoginResponse> response) {
                        if (response.code() == 200) {
                            view.showUser(response.body());
                        } else if (response.code() == 400) {
                            view.errorLogin();
                        } else {
                            view.errorEmpty();
                        }
                    }
                });
    }

    @Override
    public void rxUnsubscribe() {
        if (subscription != null) {
            if (!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    @Override
    public void setView(LoginActivityMVP.View view) {
        this.view = view;
    }
}
