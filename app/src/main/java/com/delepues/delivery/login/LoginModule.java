package com.delepues.delivery.login;


import com.delepues.delivery.http.DeliveryApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by azapata on 10/12/17.
 */
@Module
public class LoginModule {
    @Provides
    public LoginActivityMVP.Presenter providesLoginActivityPresenter(LoginActivityMVP.Model landingModel) {
        return new LoginPresenter(landingModel);
    }

    @Provides
    public LoginActivityMVP.Model provideLoginActivityModel(Repository repository) {
        return new LoginModel(repository);
    }

    @Singleton
    @Provides
    public Repository provideRepo(DeliveryApiService deliveryApiService) {
        return new LoginRepository(deliveryApiService);
    }
}
