package com.delepues.delivery.login;

import com.delepues.delivery.http.apimodel.Login;
import com.delepues.delivery.http.apimodel.LoginResponse;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/31/17.
 */

public interface LoginActivityMVP {
    interface View {
        void showUser(LoginResponse response);

        void errorLogin();

        void showProgress();

        void hideProgress();

        void errorEmpty();
    }

    interface Presenter {
        void loginUser(Login login);

        void rxUnsubscribe();

        void setView(LoginActivityMVP.View view);
    }

    interface Model {
        Observable<Response<LoginResponse>> loginUser(Login login);
    }
}
