package com.delepues.delivery.login;

import com.delepues.delivery.http.DeliveryApiService;
import com.delepues.delivery.http.apimodel.Login;
import com.delepues.delivery.http.apimodel.LoginResponse;
import com.delepues.delivery.http.apimodel.User;
import com.delepues.delivery.http.apimodel.UserGeneral;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by azapata on 10/12/17.
 */

public class LoginRepository implements Repository {
    DeliveryApiService deliveryApiService;

    public LoginRepository(DeliveryApiService deliveryApiService) {
        this.deliveryApiService = deliveryApiService;
    }

    @Override
    public Observable<Response<LoginResponse>> getResultsFromNetwork(Login login) {
        return deliveryApiService.loginUser(login);
    }

    @Override
    public Observable<Response<LoginResponse>> getResultsFromLocal(Login login) {
        User user = new User();
        user.setGeneral(new UserGeneral("a144693c-face-4b8f-8ddc-b2acf0df80e0", "369fb8be-885c-471a-8dbf-00b4664b6028", "maaburto", "moises@kokosushi.com", "Alejo", "Aburto", "Me like Test"));
        user.setDriver_id("c2e0800c-becb-4906-9ab4-1e3adc07ec57");
        user.setProfile("Admin");
        LoginResponse loginResponse = new LoginResponse(user, "deb03f8e-8339-4410-89a2-671c2b25edce", true);
        Response<LoginResponse> log = Response.success(loginResponse);

        return Observable.just(log);
    }
}
