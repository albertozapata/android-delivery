package com.delepues.delivery.root;


import com.delepues.delivery.http.ApiModule;
import com.delepues.delivery.login.LoginActivity;
import com.delepues.delivery.login.LoginModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by azapata on 9/25/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class, ApiModule.class, LoginModule.class})
public interface ApplicationComponent {

    void inject(LoginActivity target);

}
