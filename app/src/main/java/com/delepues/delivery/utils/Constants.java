package com.delepues.delivery.utils;

/**
 * Created by azapata on 9/27/17.
 */

public class Constants {
    public static String userEmail = "USER_EMAIL";
        public static String token = "TOKEN";
    public static String driverId = "DRIVER_ID";
    public static String latitude = "LATITUDE";
    public static String longitude = "LONGITUDE";
    public static String profile = "PROFILE";
}
