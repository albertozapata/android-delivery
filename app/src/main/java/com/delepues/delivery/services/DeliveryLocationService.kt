package com.delepues.delivery.services

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.delepues.delivery.BuildConfig
import com.delepues.delivery.R
import com.delepues.delivery.http.DeliveryApiService
import com.delepues.delivery.http.apimodel.LocationDriver
import com.delepues.delivery.main.MainActivity
import com.delepues.delivery.utils.Constants
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.pixplicity.easyprefs.library.Prefs
import io.socket.client.IO
import io.socket.client.Socket
import mu.KLogging
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.*
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.TimeUnit.MINUTES
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


/**
 * Created by azapata on 9/26/17.
 */
class DeliveryLocationService : Service(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private val LOCATION_INTERVAL = 5000
    private val LOCATION_DISTANCE = 8f

    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null

    internal var currentLocation: Location? = null
    //connect socket
    lateinit var socket: Socket
    lateinit var gson: Gson

    internal var myTimer_publish: Timer? = null
    internal var myTimerTask_publish: TimerTask? = null
    internal var lastUpdate: Long = 0
    var MAX_DURATION = MILLISECONDS.convert(5, MINUTES)
    private val NOTIFICATION_ID = 1520
    lateinit var notificationManager: NotificationManager
    lateinit var retrofit: Retrofit
    lateinit var token: String
    lateinit var publish: DeliveryApiService
    private var subscription: Subscription? = null

    companion object : KLogging()

    //<editor-fold desc="GoogleApiClient">
    override fun onConnected(bundle: Bundle?) {
        Log.e("Delivava", "Connected to GoogleApiClient")
        buildLocationRequest()
        buildLocationService()
    }

    override fun onConnectionSuspended(i: Int) {
        Log.e("Delivava", "ConnectionSuspended to GoogleApiClient")
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("Delivava", "ConnectionFailed to GoogleApiClient")
    }
    //</editor-fold>

    //<editor-fold desc="Service">
    override fun onBind(intent: Intent?): IBinder {
        Log.e("Delivava", "onBind Service")
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.e("Delivava", "onStartCommand Service")
        mGoogleApiClient?.connect()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onCreate() {
        super.onCreate()
        gson = Gson()
        Log.e("Delivava", "onCreate Service")
        buildRetrofit()
        buildSocketConfig()
        buildGoogleApiClient()
        //startTimer()

        notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val builder = Notification.Builder(this)
        builder.setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.notification_body_text))
                .setSmallIcon(R.drawable.ic_stat_directions_bike)
                .setContentIntent(PendingIntent.getActivity(this, 0, intent, 0))
                .setOngoing(true)
                .setAutoCancel(false)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startForeground(NOTIFICATION_ID, builder.build())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        notificationManager.cancelAll()
        subscription?.let {
            if (!subscription?.isUnsubscribed!!) {
                subscription?.unsubscribe()
            }
        }
//        myTimerTask_publish?.cancel()
//        myTimer_publish?.cancel()
//        myTimer_publish?.purge()

        mGoogleApiClient?.disconnect()
    }

    //</editor-fold>

    private fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
    }

    private fun buildSocketConfig() {
        socket = IO.socket("http://trackinglocation.skylab.vn")
        socket.on(Socket.EVENT_CONNECT, {
            socket.emit("foo", "hi")
        }).on("order-driver-event", { args ->
            val data = args[0] as JSONObject
            val `in` = Intent()
            `in`.putExtra("TYPE", data.toString())
            `in`.action = "NOW"
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(`in`)

            Log.e("Delivava", data.toString())
        }).on(Socket.EVENT_DISCONNECT) { Log.e("Delivava", "Socket IO Disconnect") }
        socket.connect()
    }

    private fun buildLocationRequest() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest?.interval = LOCATION_INTERVAL.toLong() // Update location every 4 second
        mLocationRequest?.fastestInterval = LOCATION_INTERVAL.toLong() // Update location every 4 second
        mLocationRequest?.smallestDisplacement = LOCATION_DISTANCE
    }

    @SuppressLint("MissingPermission")
    private fun buildLocationService() {
        token = Prefs.getString(Constants.token, "error")
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, object : LocationListener {
            override fun onLocationChanged(location: Location?) {
                currentLocation = location
                Prefs.putString(Constants.latitude, currentLocation?.latitude.toString())
                Prefs.putString(Constants.longitude, currentLocation?.longitude.toString())

                val login = Prefs.getString(Constants.userEmail, "empty")
                if (login != "empty") {
                    publishLocation(currentLocation?.latitude, currentLocation?.longitude)
                }

//                val `in` = Intent()
//                `in`.putExtra("TYPE", "data")
//                `in`.action = "NOW"
//                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(`in`)
//              Log.e("Delivava,""Current Location Service : ${currentLocation?.latitude}, ${currentLocation?.longitude}" }
            }
        })
    }

    private fun publishLocation(latitude: Double?, longitude: Double?) {
        latitude?.let {
            subscription = publish.publishLocation(token, LocationDriver(latitude, longitude!!), Prefs.getString(Constants.driverId, ""))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { result -> Log.e("Delivava", "Location Successful") },
                            { error -> Log.e("Delivava", "Error Location Request") }
                    )
        }
    }

    private fun buildRetrofit() {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttp = OkHttpClient.Builder().addInterceptor(interceptor).build()

        retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(
                        GsonConverterFactory.create())
                .client(okHttp)
                .baseUrl(BuildConfig.HOST)
                .build()

        publish = retrofit.create(DeliveryApiService::class.java)
    }

    private fun startTimer() {
        myTimer_publish?.let {
            return
        }

        myTimer_publish = Timer()
        myTimerTask_publish = object : TimerTask() {
            override fun run() {
                if (Prefs.getString(Constants.userEmail, "empty") != "empty") {
                    val duration = Date().time - lastUpdate
                    if (duration >= MAX_DURATION) {
                        publishLocation(currentLocation?.latitude, currentLocation?.longitude)
                        lastUpdate = Date().time
                    }
                }
            }

        }
        myTimer_publish?.schedule(myTimerTask_publish, 0, 60000 * 6)
    }
}