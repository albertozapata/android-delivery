package com.delepues.delivery.order

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.delepues.delivery.R
import java.util.*


/**
 * Created by azapata on 10/10/17.
 */

class OrdersAdapter(private val dataSet: ArrayList<String>, val context: Context, private val listener: OrdersAdapter.OnItemClickListener) : RecyclerView.Adapter<OrdersAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var order: TextView = itemView.findViewById<View>(R.id.text_number_order) as TextView
        internal var time: TextView = itemView.findViewById<View>(R.id.text_time_order) as TextView
        internal var products: TextView = itemView.findViewById<View>(R.id.text_products_order) as TextView
        internal var status: TextView = itemView.findViewById<View>(R.id.text_status_order) as TextView

        fun bind(item: String, listener: OnItemClickListener, context: Context) {

            order.text ="Orden No. 34643"
            time.text ="15min"
            products.text ="8"
            status.text ="Pendiente"

            itemView.setOnClickListener {
                listener.onItemClick(item)
            }
            //imageViewIcon.setImageDrawable(context.resources.getDrawable(vectorList[listPosition]))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_orders, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        holder.bind(dataSet[listPosition], listener, context)
    }

    override fun getItemCount(): Int = dataSet.size

    interface OnItemClickListener {
        fun onItemClick(item: String)
    }
}
